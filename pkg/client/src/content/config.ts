import { z, defineCollection, getCollection } from "astro:content";
import { uniq } from "lodash-es";

const BaseBlogPostSchema = z.object({
  title: z.string(),
  subTitle: z.string().optional(),
  showTitle: z.boolean().default(true),
  publishDate: z.string().transform((str) => new Date(str)),
  updatedDate: z
    .string()
    .transform((str) => new Date(str))
    .optional(),
  description: z.string().optional(),
  showNavTitle: z.boolean().default(true),
  heroImage: z
    .object({
      src: z.union([z.string().url(), z.string().startsWith("/")]).default("/img/default.jpg"),
      alt: z.string().default("Hero Image"),
      showHeroImage: z.boolean().default(true),
      styles: z
        .array(z.string())
        .transform((arr) => uniq(arr?.join(" ")))
        .optional(),
    })
    .default({
      src: "/img/default.jpg",
      alt: "Hero Image",
      showHeroImage: true,
    }),
  author: z.string().default("Dante Burgos"),
  authorContact: z.string().email().optional(),
  draft: z.boolean().default(false),
  sortOrder: z.number().default(0),
  footnote: z.string().optional(),
  tags: z.array(z.string()).default([]),
});

const TechBlogPostSchema = BaseBlogPostSchema.extend({});
const FoodBlogPostSchema = BaseBlogPostSchema.extend({});
const DiaryBlogPostSchema = BaseBlogPostSchema.extend({});

export type TBaseBlogPostSchema = z.infer<typeof BaseBlogPostSchema>;
export type TTechBlogPostSchema = z.infer<typeof TechBlogPostSchema>;
export type TFoodBlogPostSchema = z.infer<typeof FoodBlogPostSchema>;
export type TDiaryBlogPostSchema = z.infer<typeof DiaryBlogPostSchema>;

export const collections = {
  tech: defineCollection({ schema: TechBlogPostSchema }),
  food: defineCollection({ schema: FoodBlogPostSchema }),
  diary: defineCollection({ schema: DiaryBlogPostSchema }),
};

export type CollectionKeyType = keyof typeof collections;
// ? Below does not work as expected
// export const collectionKeys = Object.keys(collections) as CollectionKeyType[];
export const collectionKeys: CollectionKeyType[] = ['tech', 'food', 'diary'];
export const privateCollectionKeys: CollectionKeyType[] = ["diary"];

type BlogEntriesReturn = readonly [CollectionKeyType, getCollectionReturnEntry];
type getCollectionReturnEntry = ReturnType<typeof getCollection>;

export async function getAllCollections(): Promise<BlogEntriesReturn[]> {
  const blogEntries = await Promise.allSettled(
    collectionKeys.map(async (key) => {
      const result = await getCollection(key);
      return [key, result as getCollectionReturnEntry] as const;
    })
  );
  return blogEntries
    .filter((p): p is PromiseFulfilledResult<BlogEntriesReturn> => p.status === "fulfilled")
    .map(({ value }) => value);
}

export const convertPostQueryToSearchLink = (v: getCollectionReturnEntry) => ({
  ...v.data,
  tags: [v.collection, ...v.data.tags],
  slug: v.slug,
  collection: v.collection,
});

export async function getAllCollectionPosts(): Promise<
  Array<TBaseBlogPostSchema & { slug: string; collection: CollectionKeyType }>
> {
  return (await getAllCollections())
    .flatMap((v) => v[1])
    .map(convertPostQueryToSearchLink);
}
