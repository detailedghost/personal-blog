// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = process.env.ASTRO_SITE_TITLE ?? "Detailedghost Blog";
export const SITE_DESCRIPTION = process.env.ASTRO_SITE_DESCRIPTION ?? "Dante Burgos' personal musings";
export const SITE_URL = process.env.ASTRO_SITE_URL ?? "http://localhost:3000"
