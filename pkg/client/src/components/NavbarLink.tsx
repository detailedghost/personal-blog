import type { Component } from 'solid-js';

export interface NavbarLinkProps {
  href: string;
  isActive: boolean;
  children?: unknown;
}

export default function NavbarLink({
  href,
  children,
  isActive = false,
}): Component<NavbarLinkProps> {
  const baseClass =
    "text-teal-300 hover:text-teal-100 hover:border-fuchsia-400 transition-all text-2xl md:text-xl pb-1 w-full";
  const activeClass = "border-b-8 border-fuchsia-400";
  const defaultClass = "border-b-8 border-violet-600";
  const fullClass = isActive
    ? `${baseClass} ${activeClass}`
    : `${baseClass} ${defaultClass}`;

  return (
    <a href={href} class={fullClass}>
      {...children}
    </a>
  );
}
