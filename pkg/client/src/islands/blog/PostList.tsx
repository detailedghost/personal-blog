import { For } from "solid-js";
import type { TBaseBlogPostSchema } from "../../content/config";
import PostLinkItem from "./PostLinkItem";

export type PostListProps = {
  links: TBaseBlogPostSchema[];
  title?: string;
}

export default function PostList(props: PostListProps) {
  return (
    <section class="mt-4">
      <h1 className="text-3xl border-b-1">{props.title ?? "Posts"}</h1>
      <ul className="md:flex">
        <For each={props.links} fallback={<span>None Found.</span>}>
          {(l, i) => (
            <li key={"post-link-" + i} className="md:flex-grow md:w-1/2 md:m-1">
              <PostLinkItem link={l} />
            </li>
          )}
        </For>
      </ul>
    </section>
  );
}
