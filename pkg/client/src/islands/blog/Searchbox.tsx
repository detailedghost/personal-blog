import { createSignal, onCleanup } from "solid-js";
import type { Component, ParentProps } from "solid-js";

type SearchBoxProps = ParentProps<{
  onChange: (arg: string) => void;
}>;

export default function SearchBox(props: SearchBoxProps): Component<unknown> {
  const [searchTerm, setSearchTerm] = createSignal("");
  const onKeyDown = ({
    target,
  }: KeyboardEvent & { target: HTMLInputElement }) => {
    setSearchTerm(target.value);
    props.onChange(searchTerm());
  };
  onCleanup(() => setSearchTerm(""));
  return (
    <>
      <input
        class="w-full p-2 border-2 border-color-gray-400 rounded-md"
        type="text"
        placeholder="Search..."
        onKeyUp={onKeyDown}
      >
        {searchTerm()}
      </input>
    </>
  );
}
