import type { Component } from "solid-js";
import type { TBaseBlogPostSchema } from "../../content/config";
import { z } from "astro/zod";

type PostLinkItemProps = {
  link: TBaseBlogPostSchema;
}

export default function PostLinkItem({ link }): Component<PostLinkItemProps> {
  const heroImageLoc = z.string(link.heroImage.src).isURL
    ? link.heroImage.src
    : new URL(link.heroImage.src, window.location.origin).toString()
  const linkUrl = new URL(`/blog/${link.collection}/${link.slug}`, window.location.origin)
  return (
    <a data-id={crypto.randomUUID()} href={linkUrl.href}>
      <section
        class="bg-indigo-300 my-2 relative bg-fixed bg-contain bg-origin-border bg-position-b shadow-xl rounded-sm"
        style={`background-image: url('${heroImageLoc}')`}
      >
        <div
          class="p-4 text-white"
          style={{
            backgroundColor: "rgba(0,0,0,.6)",
          }}
        >
          <header class="text-lg ">{link.title}</header>
          <span class="overflow-ellipsis overflow-hidden">{link.description}</span>
          <footer>
            <small>Posted: {link.publishDate.toLocaleString()}</small>
          </footer>
        </div>
      </section>
    </a>
  );
}
