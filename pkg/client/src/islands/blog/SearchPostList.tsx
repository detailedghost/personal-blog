import SearchBox from "./Searchbox";
import { createSignal } from "solid-js";
import { debounce } from "lodash-es";
import type { TBaseBlogPostSchema } from "../../content/config";
import type { Component } from "solid-js";
import PostList, { PostListProps } from "./PostList";

export type SearchPostListProps = {
  links: TBaseBlogPostSchema[];
  sortByFn?: (a: TBaseBlogPostSchema, b: TBaseBlogPostSchema) => TBaseBlogPostSchema[];
} & Omit<PostListProps, "links">;

const defaultSortByFn = (a: TBaseBlogPostSchema, b: TBaseBlogPostSchema) =>
  b.publishDate.getTime() - a.publishDate.getTime();

export default function SearchPostList(props: SearchPostListProps): Component<SearchPostListProps> {
  const defaultLinks = props.links.sort(defaultSortByFn);
  const [posts, updatePosts] = createSignal(defaultLinks);

  const setSearchTerm = debounce((searchTerm?: string) => {
    if (!searchTerm) {
      updatePosts(defaultLinks);
      return;
    }

    const target = searchTerm.toLowerCase();
    updatePosts(
      posts().filter(
        (i: TBaseBlogPostSchema) =>
          i.title.toLowerCase().indexOf(target) > -1 ||
          i.tags.includes(target) ||
          (i.description && i.description.toLowerCase()?.indexOf(target) > -1)
      )
    );
  }, 300);

  return (
    <>
      <SearchBox onChange={setSearchTerm} />
      <PostList title={props.title} links={posts()} />
    </>
  );
}
