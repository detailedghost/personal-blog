import type { Component } from "solid-js";
import dayjs from 'dayjs'

type DateTimeDisplayProps = {
  datetime: string | Date;
  includeTime?: boolean;
};

export default function DateTimeDisplay({ datetime, includeTime = false }): Component<DateTimeDisplayProps> {
  const date = dayjs(datetime).locale(window.navigator?.language ?? 'en-US');

  if(includeTime) {
    return <datetime>{date.format('MM/DD/YYYY hh:mm A')}</datetime>;
  }
  return <date>{date.format('MM/DD/YYYY')}</date>;
}
