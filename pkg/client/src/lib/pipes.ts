export const LocaleDT = (date: string): string =>
  date ? new Date(Date.parse(date)).toLocaleString() : "";
