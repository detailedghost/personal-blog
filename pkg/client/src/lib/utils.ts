export const isUrl = (target: unknown): target is object | string =>
  !!target && typeof target !== "string" && "href" in (target as object);
