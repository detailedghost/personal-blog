const NAV_PATH_LINKS = [
  ["", "Home"],
  ["blog", "Blog"],
  //["about", "About"],
];

export const getNavLinks = (baseUrl: URL): [URL, string][] => {
  return NAV_PATH_LINKS.map((l) => [
    new URL(`${baseUrl.origin}/${l[0].toLowerCase()}`),
    l[1],
  ]);
};
