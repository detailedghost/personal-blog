import { defineConfig } from "astro/config";
import mdx from "@astrojs/mdx";
import sitemap from "@astrojs/sitemap";
import tailwind from "@astrojs/tailwind";
import solidJs from "@astrojs/solid-js";
import image from "@astrojs/image";

// https://astro.build/config
export default defineConfig({
  site: process.env.ASTRO_SITE_URL ?? "http://localhost:3000",
  markdown: {
    drafts: true,
  },
  integrations: [
    mdx({
      gfm: true,
      drafts: true,
    }),
    sitemap({
      filter: (page) => !/http.*\/diary\/*/.test(page),
      entryLimit: 1_000_000,
    }),
    tailwind(),
    solidJs(),
    image(),
  ],
});
